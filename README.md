# Lazada React Shop

This is my source code for Lazada Senior Front End developer assignment.

Demo: https://t3-universal-react-shop-uxlfjgwlxx.now.sh

In this assignment, I have challenged myself with these techniques:

- Hot module reload: for this, I had to prepare a custom dev server based on Express with custom Webpack configuration.
- Server side rendering: Although I could have achieved this using Next.js or a Universal React boilerplate, I attempted to re-implement this myself with custom Express setup. However, I ran out of time budget for the server config part, so what I have done now is just simple static server side rendering (without the data fetching part which may cost me another day).
- Responsive design: although the assignment doesn't mention responsive website, I have implemented a responsive website that work across devices.
- Page transition: I'm experimenting with this technique namely [FLIP animation](https://aerotwist.com/blog/flip-your-animations/), using `react-flip-toolkit`, it came to my realization that doing it with async data fetching is much harder and require some additional hooks. So right now it is left half-done.

Notes: for other "good to have" remarks, I could have done all of them but I spent much time on the hot module reload and server side rendering that I have no time left.

## Getting started:

```sh
npm install
npm start
```

Open browser at port 3000.
