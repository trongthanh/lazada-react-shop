/* © 2018 Tran Trong Thanh */
import React, { Component } from 'react';
import { ScrollBox } from 'react-scroll-box';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

class Header extends Component {
	state = {
		catListHeight: 40,
	};

	componentDidMount() {
		this.updateCatListHeight();
	}

	componentWillUpdate(nextProps) {
		if (this.props.viewportWidth !== nextProps.viewportWidth) {
			this.updateCatListHeight();
		}
	}

	catList = React.createRef();

	updateCatListHeight() {
		const catListRect = this.catList.current.getBoundingClientRect();
		// console.log('this.catList.current:', this.catList.current);
		console.log('catListRect.height:', catListRect.height);

		// eslint-disable-next-line react/no-did-mount-set-state
		this.setState({
			catListHeight: catListRect.height,
		});
	}

	render() {
		return (
			<header className="site-header container">
				<ul className="top-links container__content">
					<li className="top-links__item">
						<a className="top-links__link" href="#">
							Save More On App
						</a>
					</li>
					<li className="top-links__item">
						<a className="top-links__link" href="#">
							Sell on lazada
						</a>
					</li>
					<li className="top-links__item">
						<a className="top-links__link" href="#">
							Customer Care
						</a>
					</li>
					<li className="top-links__item">
						<a className="top-links__link" href="#">
							Track my order
						</a>
					</li>
					<li className="top-links__item">
						<a className="top-links__link" href="#">
							Login
						</a>
					</li>
					<li className="top-links__item">
						<a className="top-links__link" href="#">
							Signup
						</a>
					</li>
					<li className="top-links__item">
						<a className="top-links__link" href="#">
							Thay Đổi Ngôn Ngữ
						</a>
					</li>
				</ul>
				<div className="site-id container__content">
					<Link className="site-id__home-link" to="/">
						<img className="site-id__logo" src="/static/img/lazada-logo.png" alt="Lazada.vn" />
					</Link>
					<form className="site-id__search search-box">
						<input className="search-box__text" type="text" placeholder="Search for anything on Lazada" />
						<input className="search-box__btn button button--primary" type="submit" value="Search" />
					</form>
					<a className="site-id__cart" href="#" title="Shopping cart">
						<i className="material-icons site-id__cart-icon">shopping_basket</i> Cart
					</a>
				</div>
				<div className="container__content">
					<ScrollBox disableScrollY style={{ height: `${this.state.catListHeight}px` }}>
						<nav className="cat-list" ref={this.catList}>
							<Link className="cat-list__item" to="/" title="mock menu, no action">
								Nursery
							</Link>
							<Link className="cat-list__item" to="/" title="mock menu, no action">
								Feeding
							</Link>
							<Link className="cat-list__item cat-list__item--active" to="/" title="mock menu, no action">
								Clothing &amp; Accessories
							</Link>
							<Link className="cat-list__item" to="/" title="mock menu, no action">
								Diapering &amp; Potty
							</Link>
							<Link className="cat-list__item" to="/" title="mock menu, no action">
								Baby Personal&nbsp;Care
							</Link>
							<Link className="cat-list__item" to="/" title="mock menu, no action">
								Baby Gear
							</Link>
							<Link className="cat-list__item" to="/" title="mock menu, no action">
								Baby &amp; Toddler&nbsp;Toys
							</Link>
							<Link className="cat-list__item" to="/" title="mock menu, no action">
								Figures &amp; Collectibles
							</Link>
							<Link className="cat-list__item" to="/" title="mock menu, no action">
								Sports &amp; Outdoor&nbsp;Play
							</Link>
							<Link className="cat-list__item" to="/" title="mock menu, no action">
								Remote&nbsp;Control &amp; Vehicles
							</Link>
						</nav>
					</ScrollBox>
				</div>
			</header>
		);
	}
}

const mapStateToProps = state => ({ viewportWidth: state.browser.viewportWidth });

export default connect(mapStateToProps)(Header);
