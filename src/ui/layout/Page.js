/* © 2018 Tran Trong Thanh */
import React from 'react';
import Header from './Header';
import Footer from './Footer';

const Page = ({ children }) => (
	<>
		<Header />
		{children}
		<Footer />
	</>
);

export default Page;
