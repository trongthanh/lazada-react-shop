/* © 2018 Tran Trong Thanh */
/* eslint react/jsx-no-bind: off*/
import React from 'react';

const Footer = () => {
	const onSubmit = event => {
		event.preventDefault();
		alert('Thank you for submission. This is just an example web page');
	};

	return (
		<div className="container">
			<footer className="footer container__content">
				<section className="footer__section-contact">
					<h3 className="footer__title">CONTACT US</h3>
					<ul className="footer__links">
						<li>
							<a href="#">
								Mon - Sun: 9am - 6pm(Hotline),
								<br />
								7am-10pm (Online chat)
							</a>
						</li>
						<li>
							<a href="#">Help Center</a>
						</li>
						<li>
							<a href="#">How to Buy</a>
						</li>
						<li>
							<a href="#">Shipping & Delivery</a>
						</li>
						<li>
							<a href="#">International Product Policy</a>
						</li>
						<li>
							<a href="#">How to Return</a>
						</li>
						<li>
							<a href="#">Voucher usage</a>
						</li>
					</ul>
				</section>

				<section className="footer__section-about">
					<h3 className="footer__title">LAZADA VIETNAM</h3>
					<ul className="footer__links">
						<li>
							<a href="#">About Lazada</a>
						</li>
						<li>
							<a href="#">Sell on Lazada</a>
						</li>
						<li>
							<a href="#">Afﬁliate Program</a>
						</li>
						<li>
							<a href="#">Careers</a>
						</li>
						<li>
							<a href="#">Terms & Conditions</a>
						</li>
						<li>
							<a href="#">Privacy Policy</a>
						</li>
						<li>
							<a href="#">Campaign Terms & Conditions</a>
						</li>
						<li>
							<a href="#">Press & Media</a>
						</li>
						<li>
							<a href="#">Operation Regulation</a>
						</li>
						<li>
							<a href="#">Redmart</a>
						</li>
					</ul>
				</section>
				<section className="footer__section-subscriber">
					<h3 className="footer__title">EXCLUSIVE DEALS AND OFFERS!</h3>
					<form className="subscription" onSubmit={onSubmit}>
						<label className="subscription__label">
							<input
								className="subscription__email"
								type="email"
								name="subscriber-email"
								placeholder="Your email address"
							/>
						</label>
						<span className="subscription__radio-group">
							<label className="subscription__label">
								<input type="radio" name="subscriber-gender" value="male" /> Male
							</label>
							<label className="subscription__label">
								<input type="radio" name="subscriber-gender" value="female" /> Female
							</label>
						</span>
						<input className="subscription__submit" type="submit" value="Sign Me Up" />
					</form>
					<h3 className="footer__title">MOBILE APP</h3>
					<div>
						<a href="#" className="badge-appstore">
							<img src="/static/img/badge-app-store.svg" />
						</a>
						<a href="#" className="badge-appstore">
							<img src="/static/img/badge-google-play.png" />
						</a>
					</div>
				</section>
			</footer>

			<div className="container__content copyright">&copy; 2018 Tran Trong Thanh</div>
		</div>
	);
};

export default Footer;
