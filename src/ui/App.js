/* © 2018 Tran Trong Thanh */
import React, { Component } from 'react';
import { Route, withRouter } from 'react-router-dom';
import { Flipper } from 'react-flip-toolkit';
import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';
import debounce from 'lodash/debounce';

import CategoryPage from './page/CategoryPage';
import ProductPage from './page/ProductPage';

import { windowResize } from '../redux/actions/browserActions';

let isClient = true;
// create a mock window if it is server side
if (typeof window === 'undefined') {
	isClient = false;
}

class App extends Component {
	constructor(props) {
		super(props);
		// get viewportWidth the first time
		if (isClient) {
			props.windowResizeListener(window);
		}
	}

	componentDidMount() {
		if (isClient) {
			window.addEventListener('resize', this.resizeHandler);
		}
	}

	componentDidUpdate(prevProps) {
		if (isClient && this.props.location !== prevProps.location) {
			window.scrollTo(0, 0);
		}
	}
	componentWillUnmount() {
		if (isClient) {
			window.removeEventListener('resize', this.resizeHandler);
		}
	}

	resizeHandler = debounce(event => {
		// console.log('debounce resize handler', event);
		this.props.windowResizeListener(event.currentTarget);
	}, 200);

	render() {
		return (
			<Flipper
				flipKey={this.props.location.pathname + this.props.location.search}
				decisionData={{
					location: this.props.location,
					search: this.props.search,
				}}
			>
				<Route path="/" exact component={CategoryPage} />
				<Route path="/product/:id" component={ProductPage} />
			</Flipper>
		);
	}
}

const mapDispatchToProps = dispatch => ({
	windowResizeListener: event => dispatch(windowResize(event)),
});

export default hot(module)(
	withRouter(
		connect(
			null,
			mapDispatchToProps
		)(App)
	)
);
