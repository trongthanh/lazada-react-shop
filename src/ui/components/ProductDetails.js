/* © 2018 Tran Trong Thanh */
import React, { Component } from 'react';
import { Flipped } from 'react-flip-toolkit';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import PhotoSlider from './PhotoSlider';
import StarRating from './StarRating';
import { parseMoneyAmount, formatNumber } from '../../common/utils';

class ProductDetails extends Component {
	static propTypes = {
		itemTitle: PropTypes.string,
		itemRatingScore: PropTypes.number,
		itemReviews: PropTypes.number,
		itemPrice: PropTypes.string,
		itemDiscountPrice: PropTypes.string,
		itemDiscount: PropTypes.string,
		currency: PropTypes.string,
		description: PropTypes.string,
		images: PropTypes.arrayOf(PropTypes.string),
		thumbs: PropTypes.arrayOf(PropTypes.string),
		shopInfo: PropTypes.shape({
			shopName: PropTypes.string,
			shopAvatarImg: PropTypes.string,
			shopRating: PropTypes.number,
			shopReviews: PropTypes.number,
		}),
	};

	state = {
		itemColor: '',
		itemSize: '',
		itemQuantity: 1,
		sliderShowDots: false,
	};

	componentDidMount() {
		this.setState({ sliderShowDots: this.props.viewportWidth >= 576 });
	}

	componentWillUpdate(nextProps) {
		if (this.props.viewportWidth !== nextProps.viewportWidth) {
			this.setState({ sliderShowDots: nextProps.viewportWidth >= 576 });
		}
	}

	onInputChange = event => {
		const currentTarget = event.currentTarget;

		this.setState({
			[currentTarget.name]: currentTarget.value,
		});
	};

	onAddToCart = () => {
		alert(
			`You have added ${this.state.itemQuantity} of this item of ${this.state.itemColor}, ${
				this.state.itemSize
			} to cart`
		);
	};

	render() {
		const {
			itemId,
			itemTitle,
			itemRatingScore,
			itemReviews,
			itemPrice,
			itemDiscountPrice,
			itemDiscount,
			currency,
			description,
			images,
			thumbs,
			shopInfo,
		} = this.props;
		const { sliderShowDots } = this.state;
		const isDiscount = itemDiscountPrice && itemDiscount;
		let displayPrice = itemPrice;
		const youSaveAmount = parseMoneyAmount(itemPrice) - parseMoneyAmount(itemDiscountPrice);

		if (isDiscount) {
			displayPrice = itemDiscountPrice;
		}

		console.log('sliderShowDots:', sliderShowDots);

		return (
			<Flipped flipId={itemId}>
				<article className="product container__narrow-content">
					<section className="product__media slider">
						<Flipped flipId={`${itemId}_title`}>
							<PhotoSlider showDots={sliderShowDots} images={images} thumbs={thumbs} />
						</Flipped>
					</section>
					<section className="product__meta">
						<Flipped flipId={`${itemId}_title`}>
							<h1 className="product__title">{itemTitle}</h1>
						</Flipped>
						<h2 className="product__price">
							<Flipped flipId={`${itemId}_displayprice`}>
								<span className="product__price__display-price">
									{currency}
									{displayPrice}{' '}
								</span>
							</Flipped>
							<span className="product__price__original-price">
								{currency}
								{itemPrice}
							</span>
						</h2>
						{isDiscount ? (
							<p className="product__discount-info">
								You saved {currency}
								{formatNumber(youSaveAmount)}
								&nbsp;
								<span className="product__original-price__discount">({itemDiscount})</span>
							</p>
						) : (
							''
						)}
						{/* FIXME: DEMO, hard code for now*/}
						<div className="product__variant">
							<label className="product__variant__label">Select color</label>
							<select
								name="itemColor"
								value={this.state.itemColor}
								className="product__variant__select"
								onChange={this.onInputChange}
							>
								<option value="grey">Grey</option>
								<option value="red">Red</option>
								<option value="blue">Blue</option>
								<option value="yellow">Yellow</option>
								<option value="orange">Orange</option>
								<option value="green">Green</option>
							</select>
						</div>
						<div className="product__variant">
							<label className="product__variant__label">Select size</label>
							<select
								name="itemSize"
								value={this.state.itemSize}
								className="product__variant__select"
								onChange={this.onInputChange}
							>
								<option>Select an option</option>
								<option value="0 - 2 months">Newborn (0 - 2 months)</option>
								<option value="3 - 6 months">3 - 6 months kid</option>
								<option value="6 - 12 months">6 - 12 months kid</option>
								<option value="12 - 18 months">12 - 18 months kid</option>
								<option value="18 - 24 months">18 - 24 months kid</option>
							</select>
						</div>
						<div className="product__variant">
							<label className="product__variant__label">Quantity</label>
							<input
								name="itemQuantity"
								type="number"
								value={this.state.itemQuantity}
								min="0"
								max="99"
								onChange={this.onInputChange}
							/>
						</div>
						<div className="product__cta">
							<button
								className="product__cta__add-to-cart button--primary"
								type="button"
								onClick={this.onAddToCart}
							>
								Add to Cart
							</button>
						</div>
						<hr className="product__section-separator" />
						<div className="product__merchant">
							<p>This item is sold by:</p>
							<a className="product__merchant__link" href="#">
								<img className="product__merchant__avatar" src={shopInfo.shopAvatarImg} />
								<h3 className="product__merchant__name">{shopInfo.shopName}</h3>
								<div className="product__merchant__rating">
									<StarRating rating={shopInfo.shopRating} /> <span>({shopInfo.shopReviews})</span>
								</div>
							</a>
						</div>
					</section>
					<hr className="product__section-separator" />
					<section className="product__description">
						<h3>Description</h3>
						<div
							className="product__description__content"
							dangerouslySetInnerHTML={{ __html: description }}
						/>
						<h3>Reviews</h3>
						<div className="product__review">
							<StarRating rating={itemRatingScore} /> <span>({itemReviews})</span>
						</div>
					</section>
				</article>
			</Flipped>
		);
	}
}

const mapStateToProps = state => ({ viewportWidth: state.browser.viewportWidth });

export default connect(mapStateToProps)(ProductDetails);
