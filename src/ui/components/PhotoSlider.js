/**
 * Copied shamelessly and adapted from https://react-slick.neostack.com/docs/example/custom-paging
 */
import React from 'react';
import Slider from 'react-slick';

const PhotoSlider = ({ images, thumbs, showDots }) => {
	const settings = {
		customPaging(i) {
			return (
				<a>
					<img src={thumbs[i]} />
				</a>
			);
		},
		dots: showDots,
		dotsClass: 'slick-dots slick-thumb',
		// infinite: true,
		speed: 500,
		initialSlide: 0,
		slidesToShow: 1,
		slidesToScroll: 1,
		lazyLoad: true,
	};

	return (
		<Slider {...settings}>
			{images.map((imageUrl, index) => (
				<div className="slider-item" key={index}>
					<img className="slider-item__img" src={imageUrl} />
				</div>
			))}
		</Slider>
	);
};

export default PhotoSlider;
