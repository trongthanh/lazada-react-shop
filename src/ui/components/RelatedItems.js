/* © 2018 Tran Trong Thanh */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { itemsFetchData } from '../../redux/actions/relatedListActions.js';

import Card from '../components/Card';

class RelatedItems extends Component {
	static propTypes = {
		productId: PropTypes.string.isRequired,
		items: PropTypes.arrayOf(PropTypes.object).isRequired,
		// fetched: PropTypes.boolean.required,
		loading: PropTypes.bool.isRequired,
		// errored: PropTypes.boolean,
	};

	componentDidMount() {
		this.props.fetchData(this.props.productId);
	}

	render() {
		const { items, loading, fetched } = this.props;
		let cards;
		if (loading) {
			cards = 'Loading';
		}
		if (fetched) {
			cards = items.map(item => <Card key={item.itemId} ignoreDiscount {...item} />);
		}

		return <ul className="container__content item-grid">{cards}</ul>;
	}
}

const mapStateToProps = state => ({
	items: state.categoryList.items,
	fetched: state.categoryList.fetched,
	loading: state.categoryList.loading,
	errored: state.categoryList.errrored,
});

const mapDispatchToProps = dispatch => ({
	fetchData: itemId => dispatch(itemsFetchData(itemId)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(RelatedItems);
