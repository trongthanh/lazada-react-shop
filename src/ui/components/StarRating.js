/* © 2018 Tran Trong Thanh */
import React from 'react';
/* eslint react/no-array-index-key: off*/

const StarRating = ({ rating }) => {
	const numFullStars = Math.floor(rating);
	// console.log('numFullStars', numFullStars);

	// TODO: calculate half stars

	return (
		<span className="star-rating">
			{Array.from({ length: numFullStars }).map((v, i) => (
				<i key={i} className="material-icons">
					star
				</i>
			))}
		</span>
	);
};

export default StarRating;
