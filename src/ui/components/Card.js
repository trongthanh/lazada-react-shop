/* © 2018 Tran Trong Thanh */
import React from 'react';
import { Link } from 'react-router-dom';
import { Flipped } from 'react-flip-toolkit';

import StarRating from './StarRating';

const Card = ({
	itemId,
	itemImg,
	itemTitle,
	currency,
	itemPrice,
	itemDiscountPrice,
	itemDiscount,
	itemRatingScore,
	itemReviews,
	ignoreDiscount,
}) => {
	let displayPrice = itemPrice;

	if (itemDiscountPrice) {
		displayPrice = itemDiscountPrice;
	}

	return (
		<Flipped flipId={itemId}>
			<li className="card">
				<Link className="card__link" title={itemTitle} to={`/product/${itemId}`}>
					<div className="card__thumb">
						<Flipped flipId={`${itemId}_image`}>
							<img className="card__thumb__img" src={itemImg} />
						</Flipped>
					</div>
					<Flipped flipId={`${itemId}_title`}>
						<h3 className="card__title">{itemTitle}</h3>
					</Flipped>
					<p className="card__price-group">
						<Flipped flipId={`${itemId}_displayprice`}>
							<span className="card__display-price">
								{currency}
								{displayPrice}{' '}
							</span>
						</Flipped>
						{itemDiscountPrice && itemDiscount && !ignoreDiscount ? (
							<span className="card__original-price">
								<span className="card__original-price__price">
									{currency}
									{itemPrice}
								</span>
								&nbsp;
								<span className="card__original-price__discount">({itemDiscount})</span>
							</span>
						) : (
							''
						)}
					</p>
					{itemRatingScore ? (
						<p className="card__review">
							<StarRating rating={itemRatingScore} /> <span>({itemReviews})</span>
						</p>
					) : (
						''
					)}
				</Link>
			</li>
		</Flipped>
	);
};

export default Card;
