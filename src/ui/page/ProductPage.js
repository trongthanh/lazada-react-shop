/* © 2018 Tran Trong Thanh */
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Page from '../layout/Page';
import ProductDetails from '../components/ProductDetails';
import RelatedItems from '../components/RelatedItems';
import { productFetchData } from '../../redux/actions/productDetailsActions';

class ProductPage extends Component {
	static propTypes = {
		product: PropTypes.object,
	};
	componentDidMount() {
		const { id } = this.props.match.params;
		this.props.fetchData(id);
	}
	componentDidUpdate(prevProps) {
		if (this.props.location !== prevProps.location) {
			const { id } = this.props.match.params;
			this.props.fetchData(id);
		}
	}
	render() {
		return (
			<Page>
				<div className="container">
					<ProductDetails {...this.props.product} />
					<div className="container__narrow-content">
						<hr />
						<h3>People look at this item also bought</h3>
						{this.props.product && this.props.product.itemId ? (
							<RelatedItems productId={this.props.product.itemId} />
						) : (
							''
						)}
					</div>
				</div>
			</Page>
		);
	}
}

const mapStateToProps = state => state.productDetails;

const mapDispatchToProps = dispatch => ({
	fetchData: itemId => dispatch(productFetchData(itemId)),
});

export default withRouter(
	connect(
		mapStateToProps,
		mapDispatchToProps
	)(ProductPage)
);
