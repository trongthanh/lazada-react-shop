/* © 2018 Tran Trong Thanh */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { itemsFetchData } from '../../redux/actions/categoryListActions';

import Page from '../layout/Page';
import Card from '../components/Card';

class CategoryPage extends Component {
	static propTypes = {
		items: PropTypes.arrayOf(PropTypes.object).isRequired,
		// fetched: PropTypes.boolean.required,
		loading: PropTypes.bool.isRequired,
		// errored: PropTypes.boolean,
	};

	componentDidMount() {
		this.props.fetchData();
	}

	render() {
		const { items, loading, fetched } = this.props;
		let cards;
		if (loading) {
			cards = 'Loading';
		}
		if (fetched) {
			cards = items.map(item => <Card key={item.itemId} {...item} />);
		}

		return (
			<Page>
				<div className="container">
					<ul className="container__content item-grid">{cards}</ul>
				</div>
			</Page>
		);
	}
}

const mapStateToProps = state => ({
	items: state.categoryList.items,
	fetched: state.categoryList.fetched,
	loading: state.categoryList.loading,
	errored: state.categoryList.errrored,
});

const mapDispatchToProps = dispatch => ({
	fetchData: () => dispatch(itemsFetchData()),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(CategoryPage);
