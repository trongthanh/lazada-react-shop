/* © 2018 Tran Trong Thanh */
// By side-loading `esm` package and overload the require function,
// the rest of files this Node.js project can use ECMAScript Module (ESM) format
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
/* eslint-disable-next-line */
require = require('esm')(module /*, options*/);
require('@babel/register');

// const { default: server } = require('./server');
const { default: server } = require('./server.dev');

server.start(3000);
