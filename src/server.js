/* © 2018 Tran Trong Thanh */
import createError from 'http-errors';
import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';

// import indexRouter from './routes/index';
// import apiRouter from './routes/api';

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/static', express.static(path.join(__dirname, '/static')));
app.use('/favicon.ico', express.static(path.join(__dirname, '/static/favicon.ico')));

// app.use('/', indexRouter);
// app.use('/api', apiRouter);

app.start = PORT => {
	// Define 404 & 500 error route handler only after other middleware is defined
	// catch 404 and forward to error handler
	app.use((req, res, next) => {
		next(createError(404));
	});

	// error handler
	app.use((err, req, res) => {
		// set locals, only providing error in development
		res.locals.message = err.message;
		res.locals.error = req.app.get('env') === 'development' ? err : {};

		// render the error page
		res.status(err.status || 500);
		res.render('error');
	});
	app.listen(PORT, () => console.log('App started and listening on port', PORT));
};

export default app;
