/* © 2018 Tran Trong Thanh */

export function parseMoneyAmount(amountStr) {
	if (amountStr == null) {
		return 0;
	}

	return parseFloat(String(amountStr).replace(/[^\d.]/g, ''));
}

export function formatNumber(num, thousandSeparator = ',') {
	return String(num).replace(/\B(?=(\d{3})+(?!\d))/g, thousandSeparator);
}
