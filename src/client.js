/* © 2018 Tran Trong Thanh */
/**
 * Entry point for client's React application
 */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';

import 'react-scroll-box/target/out/index.css';
import './styles/index.scss';
import App from './ui/App';
import configureStore from './redux/store';

const store = configureStore({});

ReactDOM.render(
	<Provider store={store}>
		<Router>
			<App />
		</Router>
	</Provider>,
	document.getElementById('root')
);
