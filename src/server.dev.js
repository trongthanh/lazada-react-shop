/* © 2018 Tran Trong Thanh */
import webpack from 'webpack';
import isPlainObject from 'lodash/isPlainObject';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { StaticRouter } from 'react-router';
import { Provider } from 'react-redux';

import webpackConfig from '../config/webpack.config.dev';
import server from './server';
import configureStore from './redux/store';
import App from './ui/App';

const compiler = webpack(webpackConfig);
const host = process.env.HOST || 'localhost';
const port = process.env.PORT || 3000;

const serverOptions = {
	contentBase: `http://${host}:${port}`,
	quiet: true,
	noInfo: true,
	hot: true,
	inline: true,
	lazy: false,
	serverSideRender: true,
	publicPath: webpackConfig.output.publicPath,
	headers: { 'Access-Control-Allow-Origin': '*' },
	stats: { colors: true },
};

server.use(require('webpack-dev-middleware')(compiler, serverOptions));
server.use(require('webpack-hot-middleware')(compiler));

// Refer to https://github.com/webpack/webpack-dev-middleware#server-side-rendering
// This function makes server rendering of asset references consistent with different webpack chunk/entry configurations
function normalizeAssets(assets) {
	if (isPlainObject(assets)) {
		return Object.values(assets);
	}

	return Array.isArray(assets) ? assets : [assets];
}

//	 The following middleware would not be invoked until the latest build is finished.
server.use((req, res) => {
	const context = {};
	const store = configureStore({});

	const html = ReactDOMServer.renderToString(
		<Provider store={store}>
			<StaticRouter location={req.url} context={context}>
				<App />
			</StaticRouter>
		</Provider>
	);

	const assetsByChunkName = res.locals.webpackStats.toJson().assetsByChunkName;

	res.send(`
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="theme-color" content="#183544">

	<link rel="shortcut icon" href="/favicon.ico">
	<link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,700,800,900&amp;subset=latin-ext,vietnamese" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<link href="/main.css" rel="stylesheet">

	<title>Universal React Shop</title>
</head>
<body>
	<noscript>
		You need to enable JavaScript to run this app.
	</noscript>
	<main id="root">${html}</main>
	${normalizeAssets(assetsByChunkName.main)
		.filter(path => path.endsWith('.js'))
		.map(path => `<script src="/${path}"></script>`)
		.join('\n')}
</body>
</html>
`);
});

export default server;
