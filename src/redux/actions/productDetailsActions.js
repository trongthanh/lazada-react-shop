export function productHasErrored(bool) {
	return {
		type: 'PRODUCT_FETCH_ERRORED',
		hasErrors: bool,
	};
}

export function productIsLoading(bool) {
	return {
		type: 'PRODUCT_FETCH_LOADING',
		isLoading: bool,
	};
}

export function productFetchDataSuccess(product) {
	return {
		type: 'PRODUCT_FETCH_SUCCESS',
		product,
	};
}

export function productFetchData(itemId) {
	const url = '/static/mock-data.json';

	return dispatch => {
		dispatch(productIsLoading(true));

		fetch(url)
			.then(response => {
				if (!response.ok) {
					throw Error(response.statusText);
				}

				dispatch(productIsLoading(false));

				return response;
			})
			.then(response => response.json())
			.then(json => {
				// let's keep the service handling simple for now
				const items = json.data.resultValue['201711102'].data;
				const foundItems = items.filter(item => item.itemId === itemId);

				console.log('product details:', foundItems);

				if (foundItems.length > 0) {
					return dispatch(productFetchDataSuccess(foundItems[0]));
				}

				// no items match
				return dispatch(productHasErrored(true));
			})
			.catch(() => dispatch(productHasErrored(true)));
	};
}
