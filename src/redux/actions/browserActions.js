/* © 2018 Tran Trong Thanh */

export function windowResize(window) {
	const viewportWidth = window.innerWidth;
	const viewportHeight = window.innerHeight;
	let viewportSize = 'xs';

	if (viewportWidth < 576) {
		viewportSize = 'xs';
	} else if (viewportWidth < 768) {
		viewportSize = 'sm';
	} else if (viewportWidth < 992) {
		viewportSize = 'md';
	} else if (viewportWidth < 1200) {
		viewportSize = 'lg';
	} else {
		viewportSize = 'xl';
	}

	return {
		type: 'WINDOW_RESIZE',
		sizes: {
			viewportWidth,
			viewportHeight,
			viewportSize,
		},
	};
}
