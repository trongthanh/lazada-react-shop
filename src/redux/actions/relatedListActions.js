import sampleSize from 'lodash/sampleSize';

export function itemsHasErrored(bool) {
	return {
		type: 'ITEMS_FETCH_ERRORED',
		hasErrored: bool,
	};
}

export function itemsIsLoading(bool) {
	return {
		type: 'ITEMS_FETCH_LOADING',
		isLoading: bool,
	};
}

export function itemsFetchDataSuccess(items) {
	return {
		type: 'ITEMS_FETCH_SUCCESS',
		items,
	};
}

export function itemsFetchData(itemId) {
	const url = '/static/mock-data.json';

	return dispatch => {
		dispatch(itemsIsLoading(true));

		fetch(url)
			.then(response => {
				if (!response.ok) {
					throw Error(response.statusText);
				}

				dispatch(itemsIsLoading(false));

				return response;
			})
			.then(response => response.json())
			.then(json => {
				// let's keep the service handling simple for now
				const items = json.data.resultValue['201711102'].data;
				// console.log('in fetch items', items);
				const currentItemExcluded = items.filter(item => {
					return item.itemId !== itemId;
				});
				const fakeRelatedItems = sampleSize(currentItemExcluded, 6);

				return dispatch(itemsFetchDataSuccess(fakeRelatedItems));
			})
			.catch(() => dispatch(itemsHasErrored(true)));
	};
}
