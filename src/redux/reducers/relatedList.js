/* © 2018 Tran Trong Thanh */
const defaultState = {
	items: [],
	fetched: false,
	loading: false,
	errored: false,
};

const relatedList = (state = defaultState, action) => {
	switch (action.type) {
		case 'ITEMS_FETCH_ERRORED':
			return { ...defaultState, errored: action.hasErrored };
		case 'ITEMS_FETCH_LOADING':
			return { ...defaultState, loading: action.isLoading };
		case 'ITEMS_FETCH_SUCCESS':
			// console.log('ITEMS_FETCH_SUCCESS', action.items);

			return { items: action.items, fetched: true, loading: false, errored: false };
		default:
			return state;
	}
};

export default relatedList;
