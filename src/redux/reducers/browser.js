/* © 2018 Tran Trong Thanh */
const defaultState = {
	viewportWidth: 0,
	viewportHeight: 0,
	viewportSize: 'xs',
};

const browser = (state = defaultState, action) => {
	switch (action.type) {
		case 'WINDOW_RESIZE':
			console.log('WINDOW_RESIZE', action.sizes);
			return { ...state, ...action.sizes };
		default:
			return state;
	}
};

export default browser;
