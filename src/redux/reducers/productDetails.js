/* © 2018 Tran Trong Thanh */
const defaultState = {
	product: {
		trackInfo: '',
		isCurrencyLeft: 1,
		itemTitle: '',
		_pos_: 1,
		entityType: 'OTHER',
		clickTrackInfo: '',
		itemDiscount: '',
		itemCategoryInfos: [{}],
		acm: '',
		itemRegion: null,
		itemInstallment: {},
		itemId: '',
		itemImg: '',
		itemRatingScore: 0,
		itemReviews: 0,
		itemDiscountPrice: '',
		itemSellerSegments: [],
		currency: '₫',
		itemPrice: '0',
		itemServices: [],
		scm: '',
		itemUrl: '',
		_self_scm: '',
		height: 0,
		// below are additional fields from sample API
		description: '',
		images: [],
		thumbs: [],
		shopInfo: {
			shopName: '',
			shopAvatarImg: '',
			shopRating: 0,
			shopReviews: 0,
		},
	},
	fetched: false,
	loading: false,
	errored: false,
};

const productDetails = (state = defaultState, action) => {
	switch (action.type) {
		case 'PRODUCT_FETCH_ERRORED':
			return { ...defaultState, errored: true };
		case 'PRODUCT_FETCH_LOADING':
			return { ...defaultState, loading: true };
		case 'PRODUCT_FETCH_SUCCESS':
			return { product: action.product, fetched: true, loading: false, errored: false };
		default:
			return state;
	}
};

export default productDetails;
