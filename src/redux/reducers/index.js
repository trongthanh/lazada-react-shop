/* © 2018 Tran Trong Thanh */
import { combineReducers } from 'redux';
import browser from './browser';
import categoryList from './categoryList';
import productDetails from './productDetails';
import relatedList from './relatedList';

export default combineReducers({
	browser,
	categoryList,
	productDetails,
	relatedList,
});
