// Nau standard eslint rules, save it as .eslintrc.js
module.exports = {
	root: true,
	extends: ['nau-react'],
	rules: {
		'comma-dangle': [
			'error',
			{
				arrays: 'always-multiline',
				objects: 'always-multiline',
				imports: 'always-multiline',
				exports: 'always-multiline',
				functions: 'never',
			},
		],
		'no-param-reassign': 'off',
		'import/prefer-default-export': 'off',
		'react/prop-types': 'off',
		'react/require-default-props': 'off',
		'react/forbid-prop-types': 'off',
	},
	globals: {},
	env: {
		browser: true,
		node: true,
	},
	parser: 'babel-eslint',
	parserOptions: {
		ecmaVersion: 2018,
		sourceType: 'module',
		ecmaFeatures: {
			impliedStrict: true,
			jsx: true,
			classes: true,
		},
	},
	plugins: [],
};
